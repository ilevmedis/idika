
package gr.gov.idika.webservices.a2dplusepres_asfa;

import javax.xml.bind.annotation.XmlRegistry;


/**
 * This object contains factory methods for each 
 * Java content interface and Java element interface 
 * generated in the gr.gov.idika.webservices.a2dplusepres_asfa package. 
 * <p>An ObjectFactory allows you to programatically 
 * construct new instances of the Java representation 
 * for XML content. The Java representation of XML 
 * content can consist of schema derived interfaces 
 * and classes representing the binding of schema 
 * type definitions, element declarations and model 
 * groups.  Factory methods for each of these are 
 * provided in this class.
 * 
 */
@XmlRegistry
public class ObjectFactory {


    /**
     * Create a new ObjectFactory that can be used to create new instances of schema derived classes for package: gr.gov.idika.webservices.a2dplusepres_asfa
     * 
     */
    public ObjectFactory() {
    }

    /**
     * Create an instance of {@link EntryPoint2 }
     * 
     */
    public EntryPoint2 createEntryPoint2() {
        return new EntryPoint2();
    }

    /**
     * Create an instance of {@link EntryPoint }
     * 
     */
    public EntryPoint createEntryPoint() {
        return new EntryPoint();
    }

    /**
     * Create an instance of {@link EntryPointResponse }
     * 
     */
    public EntryPointResponse createEntryPointResponse() {
        return new EntryPointResponse();
    }

    /**
     * Create an instance of {@link EntryPoint2Response }
     * 
     */
    public EntryPoint2Response createEntryPoint2Response() {
        return new EntryPoint2Response();
    }

}
