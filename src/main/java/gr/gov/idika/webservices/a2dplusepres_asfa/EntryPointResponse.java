
package gr.gov.idika.webservices.a2dplusepres_asfa;

import javax.xml.bind.annotation.XmlAccessType;
import javax.xml.bind.annotation.XmlAccessorType;
import javax.xml.bind.annotation.XmlRootElement;
import javax.xml.bind.annotation.XmlType;


/**
 * <p>Java class for anonymous complex type.
 * 
 * <p>The following schema fragment specifies the expected content contained within this class.
 * 
 * <pre>
 * &lt;complexType>
 *   &lt;complexContent>
 *     &lt;restriction base="{http://www.w3.org/2001/XMLSchema}anyType">
 *       &lt;sequence>
 *         &lt;element name="entryPointResult" type="{http://www.w3.org/2001/XMLSchema}string" minOccurs="0"/>
 *       &lt;/sequence>
 *     &lt;/restriction>
 *   &lt;/complexContent>
 * &lt;/complexType>
 * </pre>
 * 
 * 
 */
@XmlAccessorType(XmlAccessType.FIELD)
@XmlType(name = "", propOrder = {
    "entryPointResult"
})
@XmlRootElement(name = "entryPointResponse")
public class EntryPointResponse {

    protected String entryPointResult;

    /**
     * Gets the value of the entryPointResult property.
     * 
     * @return
     *     possible object is
     *     {@link String }
     *     
     */
    public String getEntryPointResult() {
        return entryPointResult;
    }

    /**
     * Sets the value of the entryPointResult property.
     * 
     * @param value
     *     allowed object is
     *     {@link String }
     *     
     */
    public void setEntryPointResult(String value) {
        this.entryPointResult = value;
    }

}
