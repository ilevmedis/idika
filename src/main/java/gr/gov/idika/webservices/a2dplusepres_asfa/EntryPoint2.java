
package gr.gov.idika.webservices.a2dplusepres_asfa;

import javax.xml.bind.annotation.XmlAccessType;
import javax.xml.bind.annotation.XmlAccessorType;
import javax.xml.bind.annotation.XmlElement;
import javax.xml.bind.annotation.XmlRootElement;
import javax.xml.bind.annotation.XmlType;


/**
 * <p>Java class for anonymous complex type.
 * 
 * <p>The following schema fragment specifies the expected content contained within this class.
 * 
 * <pre>
 * &lt;complexType>
 *   &lt;complexContent>
 *     &lt;restriction base="{http://www.w3.org/2001/XMLSchema}anyType">
 *       &lt;sequence>
 *         &lt;element name="user_ed" type="{http://www.w3.org/2001/XMLSchema}string" minOccurs="0"/>
 *         &lt;element name="password_ed" type="{http://www.w3.org/2001/XMLSchema}string" minOccurs="0"/>
 *         &lt;element name="input_ed" type="{http://www.w3.org/2001/XMLSchema}string" minOccurs="0"/>
 *       &lt;/sequence>
 *     &lt;/restriction>
 *   &lt;/complexContent>
 * &lt;/complexType>
 * </pre>
 * 
 * 
 */
@XmlAccessorType(XmlAccessType.FIELD)
@XmlType(name = "", propOrder = {
    "userEd",
    "passwordEd",
    "inputEd"
})
@XmlRootElement(name = "entryPoint2")
public class EntryPoint2 {

    @XmlElement(name = "user_ed")
    protected String userEd;
    @XmlElement(name = "password_ed")
    protected String passwordEd;
    @XmlElement(name = "input_ed")
    protected String inputEd;

    /**
     * Gets the value of the userEd property.
     * 
     * @return
     *     possible object is
     *     {@link String }
     *     
     */
    public String getUserEd() {
        return userEd;
    }

    /**
     * Sets the value of the userEd property.
     * 
     * @param value
     *     allowed object is
     *     {@link String }
     *     
     */
    public void setUserEd(String value) {
        this.userEd = value;
    }

    /**
     * Gets the value of the passwordEd property.
     * 
     * @return
     *     possible object is
     *     {@link String }
     *     
     */
    public String getPasswordEd() {
        return passwordEd;
    }

    /**
     * Sets the value of the passwordEd property.
     * 
     * @param value
     *     allowed object is
     *     {@link String }
     *     
     */
    public void setPasswordEd(String value) {
        this.passwordEd = value;
    }

    /**
     * Gets the value of the inputEd property.
     * 
     * @return
     *     possible object is
     *     {@link String }
     *     
     */
    public String getInputEd() {
        return inputEd;
    }

    /**
     * Sets the value of the inputEd property.
     * 
     * @param value
     *     allowed object is
     *     {@link String }
     *     
     */
    public void setInputEd(String value) {
        this.inputEd = value;
    }

}
